from django.shortcuts import render
from .models import Testimonial
from .forms import TestimoniForm

# Create your views here.
def testimonial(request):
    responses = {}
    if request.method == "POST":
        form = TestimoniForm(request.POST)
        if form.is_valid():
            testimonial = Testimonial()
            testimonial.name = form.cleaned_data['name']
            testimonial.message = form.cleaned_data['message']
            testimonial.save()
        responses['form'] = form
        responses['data'] = Testimonial.objects.all()[::-1][:5]
        return render(request, 'testimonial/testimonial.html', responses)
    form = TestimoniForm()
    responses['form'] = form
    responses['data'] = Testimonial.objects.all()[::-1][:5]
    return render(request, 'testimonial/testimonial.html', responses)
