from django.test import TestCase
from .forms import TestimoniForm
from .models import Testimonial

# Create your tests here.
class TestimonialHTMLTest(TestCase):
    def test_text_exist(self):
        response = self.client.get('/testimonial/')

        self.assertIn('Testimoni', response.content.decode())
        self.assertIn('Nama:', response.content.decode())
        self.assertIn('Testimoni:', response.content.decode())

    def test_submit_button(self):
        response = self.client.get('/testimonial/')

        self.assertIn('Submit', response.content.decode())

    def test_form(self):
        response = self.client.get('/testimonial/')
        
        self.assertIn('<form', response.content.decode())
        self.assertIn('<input', response.content.decode())

class TestimonialViewsTest(TestCase):
    def test_form_valid(self):
        response = self.client.post('/testimonial/', data={
            'name': 'Ryaas',
            'message': 'Aku Senang sekali',
        })
        self.assertTrue(Testimonial.objects.filter(name="Ryaas").exists())
