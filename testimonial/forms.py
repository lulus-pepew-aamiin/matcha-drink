from django import forms

class TestimoniForm(forms.Form):
    name = forms.CharField(label="Nama", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Masukkan Nama Anda',
        'required': True,
    }))

    message = forms.CharField(label="Testimoni", widget=forms.Textarea(attrs={
        'rows': 6,
        'style': 'resize:none;',
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Testimoninya :)',
        'required': True,
    }))