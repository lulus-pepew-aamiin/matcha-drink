from django import forms

from .models import UserVisit

class User(forms.Form):
    user_name = forms.CharField(
        label='Siapa Namamu?',
        max_length=50,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        ),
    )
