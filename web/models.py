from django.db import models

from menu.models import Menu

# Create your models here.
class UserVisit(models.Model):
    user = models.CharField(max_length=50)

    def __str__(self):
        return self.user

class FavoriteMenu(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    image = models.FileField(upload_to='web/')
