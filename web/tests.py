from django.test import Client, TestCase

from .models import UserVisit, FavoriteMenu
from menu.models import Menu
from .forms import User

# Create your tests here.
class UserTest(TestCase):
    def test_user(self):
        user1 = UserVisit.objects.create(
            user = 'Algi',
        )
        self.assertIn('Algi', str(user1))

class HomepageTest(TestCase):
    def setUp(self):
        menu1 = Menu(
            name='Matchiato',
            price=18000,
            description='Sentuhan foam diatas espresso dengan gaya Eropa yang kekinian.',
            favorite=True,
            image='menu/matchiato.png',
        )
        menu1.save()
        menu2 = Menu(
            name='Matchinno',
            price=15000,
            description=(
                'Espresso dengan susu dan lapisan foam yang dalam. '
                'Minuman eksklusif MatchA Drink yang dibuat hanya untukmu.'
            ),
            favorite=True,
            image='menu/matchinno.png',
        )
        menu2.save()

        FavoriteMenu(menu=menu1, image='web/matchiato.png').save()
        FavoriteMenu(menu=menu2, image='web/matchinno.png').save()

    def test_hello_name_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_text_exist(self):
        response = self.client.get('/')
        self.assertContains(response, 'Menu Favorit')
        self.assertContains(response, 'Lihat')
        self.assertContains(response, 'Selengkapnya')

    def test_lihat_selengkapnya_button(self):
        response = self.client.get('/')
        self.assertContains(response, '<button')

    def test_price(self):
        response = self.client.get('/')
        self.assertContains(response, 'Rp 18.000', count=1)
        self.assertContains(response, 'Rp 15.000', count=1)
