from django.contrib import admin

from .models import UserVisit, FavoriteMenu

# Register your models here.
admin.site.register([UserVisit, FavoriteMenu])
