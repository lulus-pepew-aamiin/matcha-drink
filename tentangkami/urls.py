from django.urls import path
from . import views

app_name = 'tentangkami'

urlpatterns = [
    path('', views.tentang_kami, name='tentangkami'),
]
