from django.test import TestCase,Client
from .models import EmailModel
from .forms import EmailForms

# Create your tests here.
class AboutUsTest(TestCase):

    def test_about_us_url_valid(self):
        response = Client().get('/tentang_kami/')
        self.assertEqual(response.status_code,200)

    def test_about_us_template(self):
        response = self.client.get('/tentang_kami/')
        self.assertTemplateUsed(response, 'index.html')

    def test_email_form(self):
        response = self.client.get('/tentang_kami/')
        self.assertContains(response,'<button')
        self.assertContains(response,'<form')

    def test_view(self):
        email = EmailModel.objects.create(email = 'wijaya@gmail.com')
        data = {'email' : email.email}
        form = EmailForms(data = data)
        self.assertTrue(form.is_valid())

    def test_view_post(self):
        data ={ 'email' : 'ajiinisti@gmail.com'}
        response = Client().post('/tentang_kami/',data)
        self.assertEqual(response.status_code, 302)

    def test_database(self):
        email = EmailModel(email = 'ajiinisti@gmail.com')
        email.save()
        hitungjumlah = EmailModel.objects.all().count()
        self.assertEqual(hitungjumlah,1)

