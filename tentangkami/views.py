from django.shortcuts import render,redirect
from .forms import EmailForms
from .models import EmailModel

# Create your views here.
def tentang_kami(request):   
    form = EmailForms(request.POST or None)
    if request.method == 'POST':
        model = EmailModel(email = form['email'].value())
        model.save()
        return redirect("tentangkami:tentangkami")
    else:
        return render(request,"index.html", {'generated_form': form})

