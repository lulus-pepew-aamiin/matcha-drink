from django.contrib import admin

# Register your models here.
from .models import EmailModel

admin.site.register(EmailModel)
