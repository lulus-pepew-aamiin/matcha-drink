from django import forms

from .models import Menu, Order

class OrderForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for menu in Menu.objects.all():
            self.fields['menu_' + str(menu.id)] = forms.IntegerField(
                min_value=0,
                max_value=32767,
                initial=0,
            )

    def clean(self):
        super().clean()

        total = 0
        for value in self.cleaned_data.values():
            total += value
        if total == 0:
            raise forms.ValidationError('Harus pesan minimal 1 minuman.')

