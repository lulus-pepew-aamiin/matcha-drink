from django.contrib import admin

from .models import Cart, Menu, Order

# Register your models here.
admin.site.register([Cart, Menu, Order])
