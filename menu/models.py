from django.db import models

# Create your models here.
class Menu(models.Model):
    name = models.CharField(max_length=50)
    price = models.PositiveIntegerField()
    description = models.TextField()
    favorite = models.BooleanField(default=False)
    image = models.FileField(upload_to='menu/')

    def __str__(self):
        return self.name

class Cart(models.Model):
    user = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        all_orders = []
        for order in self.order_set.all():
            all_orders.append(str(order))

        return '{}: {}'.format(self.user, ', '.join(all_orders))


class Order(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    amount = models.PositiveSmallIntegerField()

    def __str__(self):
        return '{} {}'.format(self.amount, self.menu)
