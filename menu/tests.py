from django.test import TestCase

from .models import Cart, Menu, Order


# Create your tests here.
class MenuModelTest(TestCase):
    def test_model_menu(self):
        menu1 = Menu(
            name='Matchiato',
            price=18000,
            description='Sentuhan foam diatas espresso dengan gaya Eropa yang kekinian.',
            image='menu/matchiato.png',
        )
        menu1.save()
        menu2 = Menu(
            name='Matchinno',
            price=15000,
            description=(
                'Espresso dengan susu dan lapisan foam yang dalam. '
                'Minuman eksklusif MatchA Drink yang dibuat hanya untukmu.'
            ),
            image='menu/matchinno.png',
        )
        menu2.save()

        all_menu = Menu.objects.all()

        self.assertEqual(all_menu.count(), 2)
        self.assertIn(menu1, all_menu)
        self.assertIn(menu2, all_menu)
        self.assertEqual(str(menu1), menu1.name)

    def test_model_order(self):
        menu1 = Menu(
            name='Matchiato',
            price=18000,
            description='Sentuhan foam diatas espresso dengan gaya Eropa yang kekinian.',
            image='menu/matchiato.png',
        )
        menu1.save()
        cart = Cart(
            user='okto',
        )
        cart.save()

        order1 = Order(
            cart=cart,
            menu=menu1,
            amount=2,
        )
        order1.save()

        all_order = Order.objects.all()

        self.assertEqual(all_order.count(), 1)
        self.assertIn(order1, all_order)
        self.assertEqual(str(order1), '2 Matchiato')
        self.assertEqual(str(cart), 'okto: 2 Matchiato')


class MenuTest(TestCase):
    def setUp(self):
        Menu(
            name='Matchiato',
            price=18000,
            description='Sentuhan foam diatas espresso dengan gaya Eropa yang kekinian.',
            favorite=True,
            image='menu/matchiato.png',
        ).save()
        Menu(
            name='Matchinno',
            price=15000,
            description=(
                'Espresso dengan susu dan lapisan foam yang dalam. '
                'Minuman eksklusif MatchA Drink yang dibuat hanya untukmu.'
            ),
            favorite=True,
            image='menu/matchinno.png',
        ).save()
        Menu(
            name='Caffè Latte',
            price=18000,
            description=(
                'Espresso dengan susu hangat dan foam. Caffè latte memiliki '
                'rasa sesegar air gunung.'
            ),
            image='menu/caffe_latte.png',
        ).save()
        Menu(
            name='Americano',
            price=18000,
            description='Kopi espresso dengan tambahan air hangat',
            image='menu/americano.png',
        ).save()
        Menu(
            name='Espresso',
            price=18000,
            description=(
                'Selembut sutra dan memiliki bau yang menggugah selera. '
                'Espresso adalah minuman yang cocok untuk bersantai.'
            ),
            image='menu/espresso.png',
        ).save()

    def test_exist(self):
        response = self.client.get('/menu/')

        self.assertContains(response, 'Yuk Pesan')
        self.assertContains(response, 'Menu Favorit')
        self.assertContains(response, 'Menu Lainnya')

    def test_pesan_button(self):
        response = self.client.get('/menu/')

        self.assertContains(response, 'Pesan')

    def test_form_tambah(self):
        response = self.client.get('/menu/')

        self.assertContains(response, '<form')
        self.assertContains(response, '<button')
        self.assertContains(response, '<input type="number" name="menu_', count=5)

    def test_price(self):
        response = self.client.get('/menu/')

        self.assertContains(response, 'Rp 18.000', count=4)
        self.assertContains(response, 'Rp 15.000', count=1)

    def test_cart(self):
        response = self.client.post(
            '/menu/',
            data={
                'menu_1': 2,
                'menu_2': 1,
                'menu_3': 0,
                'menu_4': 0,
                'menu_5': 0,
            },
            follow=True,
        )

        self.assertEqual(Cart.objects.all().count(), 1)
        self.assertContains(response, 'Matchiato')
        self.assertContains(response, 'Rp 51.000')

    def test_cart_none(self):
        response = self.client.post(
            '/menu/',
            data={
                'menu_1': 0,
                'menu_2': 0,
                'menu_3': 0,
                'menu_4': 0,
                'menu_5': 0,
            },
            follow=True,
        )

        self.assertEqual(Cart.objects.all().count(), 0)
        self.assertContains(response, 'Harus pesan minimal 1 minuman.')
